#!/usr/bin/python

from Bio.PDB import *
from sys import argv
from math import pi
from numpy import array,where
from copy import deepcopy
from os.path import dirname,realpath

pth=dirname(realpath(__file__))

def get_psiphi(residue):
	psi_ats=[residue[x].get_vector() for x in ['N','CA','C']]
	phi_ats=[residue[x].get_vector() for x in ['C','CA','N']]
	if 'HX1' in residue:
		psi_ats.insert(0,residue['HX1'].get_vector())
		phi_ats.insert(0,residue['HX2'].get_vector())
	else:
		parent=residue.get_parent()
		resid=residue.get_id()[1]
		prev_id=resid-1
		next_id=resid+1
		if prev_id in parent:
			psi_ats.insert(0,parent[prev_id]['C'].get_vector())
		else:
			point=[x for x in ['H','HN'] if x in residue]
			if point: 
				point=residue[point[0]].get_vector()
				ca=psi_ats[1]
				n =psi_ats[0]
				can=n-ca
				npp=point-n
				r=rotaxis(pi,can)
				npp=npp.left_multiply(r)
				point=n+npp
				psi_ats.insert(0,point)
		if next_id in parent:
			phi_ats.insert(0,parent[next_id]['N'].get_vector())
		else:
			point=residue['O'].get_vector()
			ca=phi_ats[1]
			c =phi_ats[0]
			cac=c-ca
			co=point-c
			r=rotaxis(pi,cac)
			co=co.left_multiply(r)
			point=c+co
			phi_ats.insert(0,point)
	if len(psi_ats)==4: psi=calc_dihedral(*psi_ats)*180/pi
	else: psi=180
	if len(phi_ats)==4: phi=calc_dihedral(*phi_ats)*180/pi
	else: phi=180

	return psi,phi				

with open(pth+"/bbindep2015.txt",'r') as f:
	rotabase={}
	ranges={}
	data=f.readlines()[1:]
	code={'g':'0','g+':'0','t':'1','g-':'2',"Ng+":'0', "Og-":'1', "Cg-":'1','Nt':'2', 'Og+':'3','Cg+':'3','Ng-':'4','Ot':'5','Ct':'5' }
	for line in data:
		line=line.split(',')
		an=line[0]
		if an not in rotabase.keys():
			rotabase[an]=[]
			ranges[an]=[]
		rotabase[an].append({})
		rotabase[an][-1]['prob']=float(line[-1])
		rotabase[an][-1]['chi']=[]
		rotabase[an][-1]['std']=[]
		name=''
		c=[]
		for n in line[1:5]:
			n=n.strip()
			if n!='-':
				name+=code[n]
				c.append(int(code[n]))
			else: break
		if ranges[an]==[]: ranges[an]=c
		else: 
			for i in range(len(c)):
				if c[i]>ranges[an][i]: ranges[an][i]=c[i] 
		rotabase[an][-1]['name']=name
		for chi in line[5:9]:
			val,std=chi.split('(')
			val=val.strip()
			std=std.strip('()')
			if val!='-': 
				val=float(val)
				std=float(std)
	#			if val>180.0: val-=360.0
				rotabase[an][-1]['chi'].append(val)
				rotabase[an][-1]['std'].append(std)
			else: break
	rotabase['CYS']=rotabase['CYH']
	rotabase['HIP']=rotabase['HIE']
#	rotabase['GLH']=rotabase['GLU'][:]
#	rotabase['ASH']=rotabase['ASP'][:]
#PATCH NA TYR I PHE
for a in ['TYR','PHE']:
	for i in range(3):
		tmp=deepcopy(rotabase[a][0])
		tmp['chi'][0]=60.0+i*120.0
		tmp['chi'][1]=300.0
		tmp['name']='%i2'%i
		rotabase[a].append(tmp)

with open(pth+'/rotamery-def','r') as f:
	rotadef={}
	for line in f:
		line=line.split()
		aa=line[0]
		rot=line[1:]
		if aa not in rotadef.keys():rotadef[aa]=[]
		rotadef[aa].append(rot)

ranges['HIP']=ranges['HIE']
for I,J in zip(['HIE','HIP','HID'],['HSE','HSP','HSD']):
	rotadef[J]=rotadef[I]
	ranges[J]=ranges[I]

def get_rot(residue,backbone=False):
			'''
Input: Residue object (Bio.PDB)
Output: label,dihs
		label: label defining rotamer
		dihs:  array of dihedral angles (degrees) defining rotamer
			'''
			#rotadef required
			lab=''
			katy=[]
			i=0
			ifrot=False
			if residue.resname=='GLY' or backbone: 
				rsn='GLY'
				kats=get_psiphi(residue)
			else: rsn=residue.resname
			rng=ranges[rsn]
			for rt in rotadef[rsn]:
				if residue.resname!='GLY' and not backbone:
					ats=[residue[x].get_vector() for x in rt]
					kat= calc_dihedral(*ats)*180/pi
				else:
					kat=kats[rotadef[rsn].index(rt)]
				if kat<0:kat+=360
#				if residue.resname in ['TYR', 'PHE']:
#					if i==1 and kat>=240:
#						kat-=180.0
#						ifrot=True
#					elif ifrot:
#						kat=(kat+180)%360
				ltr=kat*(rng[i]+1)/360
				if residue.resname=='TYR' and i==2: 
					ltr= int(round(ltr,0))
					if ltr==6:ltr=0
				else: ltr=int(ltr)
				lab+='%i'%ltr
				katy.append(kat)
				i+=1
			return lab,array(katy)

def _next(rsn,label):
	rng=ranges[rsn]
	ret=[]
	for i in range(len(rng)):
		n=label[i]+1
		if n>rng[i]:n=0
		ret.append(n)
	return ret

def _prev(rsn,label):
	rng=ranges[rsn]
	ret=[]
	for i in range(len(rng)):
		n=label[i]-1
		if n<0:n=rng[i]
		ret.append(n)
	return ret

begs={2:60.0,5:30.0}

def _genbox(residue):
	lab,katy=get_rot(residue)
	rsn=residue.resname
	if rsn in ['TYR','THR','SER','CYS']:
		n=ranges[rsn][-1]
		lab2=lab[:-1]
		end=int(lab[-1])
		params=[deepcopy(x) for x in rotabase[rsn] if x['name']==lab2][0]
		params['chi']+=[begs[n]+end*(360.0/(n+1))]
		if rsn=='TYR':
			k=katy[-1]
			k=(k+30.0)%360
			katy[-1]=k
	else:
		params=[x for x in rotabase[rsn] if x['name']==lab][0]
	lab=map(int,list(lab))
	prev=_prev(rsn,lab)
	nxt =_next(rsn,lab)
	chi=array(params['chi'])
	pos= katy>=chi
	mins=where(pos,lab,prev)
	maxs=where(pos,nxt,lab)
	return zip(mins,maxs)

def _nolab(vlab):
	t=''
	for s in vlab:	t+=int(bool(int(s)))
	return t

def _perm(n): # generuje wierzcholki wieloscianu
	wyn=[]
	np=2**n
	
	for i in range(np):
		a=bin(i)[2:]
		a=list('0'*(n-len(a))+a)
		wyn.append(map(int,a))
				
	return wyn

def _x2code(arr): # returns string describing vertex
	  r=''
	  for x in arr:r+='%i'%int(round(x,0))
	  return r

def boxdc(residue,oh=False):
	min_max=_genbox(residue)
	if oh: 
		min_max=min_max[-1:]
		lab,katy=get_rot(residue)
		lab=lab[:-1]
	n=len(min_max)
	vertices=_perm(n)
	ret={}
	for v in vertices:
		x=map(lambda x,y:x[y],min_max,v)
		val=_x2code(x)
		if oh:val=lab+val
		ret[_x2code(v)]=val
	return ret

# Na razie boxdc wywoluje genrot, ale nie zwraca lab i katow
# w pozniejszym algorytmie bedzie trzeba powtarzac operacje!
# na razie zostawiam

#do testow jeszcze nie uwzlgendiam OH, pozniej odhaszuje

for a in ['THR','CYS','SER']: ranges[a].append(2)
ranges['TYR'].append(5)
ranges['TYR'][1]=2
ranges['PHE'][1]=2
ranges['GLH']=ranges['GLU'][:]
ranges['ASH']=ranges['ASP'][:]
ranges['GLH'][-1]=5
ranges['ASH'][-1]=5
ranges['GLY']=[5,5]
#for a in ['TYR','PHE']: ranges[a][1]=2
p=PDBParser(QUIET=True)
if __name__=='__main__':
	plik=argv[1]
	prot=p.get_structure('prot',plik)
	
	res=prot[0].child_list[0].child_list[0]
	lab,katy=get_rot(res)
#	box=boxdc(res)
	print lab
	print katy
#	print [x for x in rotabase[res.resname] if x['name']==lab][0]['chi']
#	print box
