#!/usr/bin/python

from pymolecule import multipoles as mtp
import numpy as np
import cPickle as pic
from os.path  import exists as is_present
from math import pi,sin,cos,sqrt,atan2,asin,acos,exp,log
from copy import deepcopy

bhr=0.52917721092

def drop(mol,filname):
	N=len(mol.coor)
	plik=open(filname,'w')
	plik.write('%i\n\n'%N)
	for i in range(N):
		plik.write('%5s %15.6f %15.6f %15.6f\n'%((mol.elements[i],)+tuple(mol.coor[i]*bhr)))
	plik.close() 



def kabsch(P, Q):
    """
    The optimal rotation matrix U is calculated and then used to rotate matrix
    P unto matrix Q so the minimum root-mean-square deviation (RMSD) can be
    calculated.
    Using the Kabsch algorithm with two sets of paired point P and Q,
    centered around the center-of-mass.
    Each vector set is represented as an NxD matrix, where D is the
    the dimension of the space.
    The algorithm works in three steps:
    - a translation of P and Q
    - the computation of a covariance matrix C
    - computation of the optimal rotation matrix U
    http://en.wikipedia.org/wiki/Kabsch_algorithm
    Parameters:
    P -- (N, number of points)x(D, dimension) matrix
    Q -- (N, number of points)x(D, dimension) matrix
    Returns:
    U -- Rotation matrix
    """

    # Computation of the covariance matrix
    C = np.dot(np.transpose(P), Q)

    # Computation of the optimal rotation matrix
    # This can be done using singular value decomposition (SVD)
    # Getting the sign of the det(V)*(W) to decide
    # whether we need to correct our rotation matrix to ensure a
    # right-handed coordinate system.
    # And finally calculating the optimal rotation matrix U
    # see http://en.wikipedia.org/wiki/Kabsch_algorithm
    V, S, W = np.linalg.svd(C)
    d = (np.linalg.det(V) * np.linalg.det(W)) < 0.0

    if d:
        S[-1] = -S[-1]
        V[:, -1] = -V[:, -1]

    # Create Rotation matrix U
    U = np.dot(V, W)

    return U


def centroid(X):
    """
    Calculate the centroid from a vectorset X
    """
    C = sum(X)/len(X)
    return C




def m2e(matrix):
    '''Converts matrix to euler angles''' 
    (alfa,beta,gamma)=(0,0,0)
#ROZWAZYC OSOBLIWOSCI!!
#   if matrix[1][0]==0:
#		if matrix[2][0]==-1:
#			print "OSOBLIWOSC"
#			alfa=atan2(matrix[2][1],matrix[1][1])
#			beta=pi/2
#			gamma=pi/2
#		elif matrix[1][0]==1:
#			print "OSOBLIWOSC"
#			alfa=atan2(matrix[2][1],matrix[1][1])
#			beta=-pi/2
#			gamma=-pi/2
#  else:
    alfa=atan2(matrix[2][1],matrix[2][2])
    beta=-asin(matrix[2][0])
    gamma=atan2(matrix[1][0],matrix[0][0])
  
    return [alfa,beta,gamma]

def fit(ref,fit,ids=[0,1,2],prms=False):
	'''Fit CAMM from library to given coors'''
#ref- cel, fit- z bazy
	pars=[]
	camm=deepcopy(fit)

	ref_coor=np.array([ref[i] for i in ids])/0.5291772  #WARNING!! Assuming ANGS!
	fit_coor=np.array([fit.coor[i] for i in ids])

	ref_c=centroid(ref_coor)
	fit_c=centroid(fit_coor)
	pars+=[fit_c,ref_c]

	ref_coor-=ref_c
	fit_coor-=fit_c

	tr=ref_c-fit_c

	mtx=kabsch(fit_coor,ref_coor)
	katy=m2e(mtx.T)
	pars+=[katy]
	camm.rotate(*katy,P=fit_c)
	camm.translate(tr)

	if not prms:
		return camm
	else:
		return camm,pars

def fit2(ref,fit,ids=[0,1,2],prms=False):
	'''Fit CAMM from library to given coors'''
#ref- cel, fit- z bazy
	pars=[]
	camm=deepcopy(fit)

	ref_coor=np.array([ref[i] for i in ids])/0.5291772  #WARNING!! Assuming ANGS!
	fit_coor=np.array([fit.coor[i] for i in ids])

	ref_c=centroid(ref_coor)
	fit_c=centroid(fit_coor)
	pars+=[fit_c,ref_c]

	ref_coor-=ref_c
	fit_coor-=fit_c

	tr=ref_c-fit_c

	mtx=kabsch(fit_coor,ref_coor)
	katy=m2e(mtx.T)
	pars+=[katy]
#	camm.rotate(*katy,P=fit_c)
	camm.translate(-fit_c)
	camm.coor=camm.coor.dot(mtx)
	camm.translate(ref_c)

	if not prms:
		return camm
	else:
		return camm,pars

def rotrans(mol,pars):
	fitc,refc,katy=pars
	ret=deepcopy(mol)
	ret.rotate(*katy,P=fitc)
	ret.translate(refc-fitc)
	return ret
