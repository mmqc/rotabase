#!/usr/bin/python

from _rotacamm import read_camm,load_camm, drop, chis, dump, load,m as multipoles, print_ref,phantom_gly
from _rotanal import PDBParser,Selection,get_rot,get_psiphi
from os.path import dirname as _dir,realpath as _path
#make path...
__version__='1.3'
__doc__='''
base  -Dict containing rotabase
avaiable functions: read_camm,drop,dump,load,multipoles,print_ref,PDBParser,Selection,get_rot,load_base
'''
_bases={'HF':{'6-31Gd':'rotabase.pkl.gz'},'MP2':{'6-31Gd':'rotabase-mp2.pkl.gz'}}
_glys={'HF':{'6-31Gd':'glys.pkl.gz'},'MP2':{'6-31Gd':'glys-mp2.pkl.gz'}}
_avail='\n'.join(['%s:  %s'%(_key,' '.join(_bases[_key].keys())) for _key in _bases ])

def load_base(method='HF',basis_set='6-31Gd'):
 try:
	ret=load(_dir(_path(__file__))+'/data/'+_bases[method][basis_set])
	gl =load(_dir(_path(__file__))+'/data/'+_glys[method][basis_set])
	for K in gl: ret[K]=gl[K]
	if 'G' in ret:ret['GLY']=ret['G']
	return ret
 except KeyError:
	print 'Unrecognized combination, available bases:\n\n',_avail
load_base.__doc__="Loads dictionary containing CAMMs for rotamers. Available bases:\n%s"%(_avail)

def restrict_base(base,L=4):
	nums={4:35}
	inmax=nums[L]
	for rsn in base:
		if type(base[rsn])==type({}):
			for roti in base[rsn]:
				base[rsn][roti].CAMM=base[rsn][roti].CAMM[:,:inmax]
		else:
			base[rsn].CAMM=base[rsn].CAMM[:,:inmax]


base=load_base()

def __test_restrict():
	a=base['ARG']['0000']
	b=base['GLU']['000']
	print 'full-ene:  ',multipoles.energy(a,b)*627.5
	print 'l4-ene  :  ',multipoles.energy(a,b,4)*627.5
	restrict_base(base)
	print 'cut     :  ',multipoles.energy(a,b)*627.5

class _tests_PDB():
	def __init__(self):
		from time import time
		self.time=time
		p=PDBParser(QUIET=True)
		prot=p.get_structure('p','tests/active.pdb')
		self.prot=Selection.unfold_entities(prot,'R')
		self.test_no=0
	def load_roti(self):
		GLU=self.prot[-1]
		t=self.time()
		a=load_camm(GLU,'ARG','0000',base)
		drop(a,'tests/ARG-0000-%i.xyz'%self.test_no)
		t=self.time()-t
		print 'standard load    :',t
		t=self.time()
		a=load_camm(GLU,'ARG','0000',base,True)
		t=self.time()-t
		print 'coor-only load   :',t
		print 'Done'
		b=load_camm(GLU,'ARG','2021',base)
		drop(b,'tests/ARG-2021-%i.xyz'%self.test_no)
		self.test_no+=1

if __name__=='__main__':
	T=	_tests_PDB()
	T.load_roti()
	__test_restrict()
	T.load_roti()

