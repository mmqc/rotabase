#!/usr/bin/python

from pymolecule import multipoles as m
from _topologie import *
from math import asin
import gzip
import cPickle as pic
from sys import argv
from _rotanal import *
from _fit_lib import *
from numpy import cos,array

def load(name):
	'''Loads Gzipped pickle file'''
	with gzip.open(name,'rb') as f:return pic.load(f)

def dump(obj,filename):
	'''Writes serialized object to gzipped pickle file of given filename'''
	with gzip.open(filename,'wb') as f:pic.dump(obj,f)

def _nolab(vlab):
	t=''
	for s in vlab:	t+='%i'%int(bool(int(s)))
	return t

def drop(mol,filename):
	'''Writes XYZ coordinates of Pymolecule object to a given filename'''
	N=len(mol.coor)
	with open(filename,'w') as f:
		f.write('%i\n\n'%N)
		for i in range(N):
			f.write('%5s %8.3f %8.3f %8.3f\n'%((mol.elements[i],)+tuple(mol.coor[i]*0.5291772)))

def dih_calc(mol,qa):
	b1=mol.coor[qa[1]]-mol.coor[qa[0]]
	b2=mol.coor[qa[2]]-mol.coor[qa[1]]
	b3=mol.coor[qa[3]]-mol.coor[qa[2]]

	n1=cross(b1,b2)
	n2=cross(b2,b3)

	n1=n1/sqrt(dot(n1,n1))
	n2=n2/sqrt(dot(n2,n2))

	b2=b2/sqrt(dot(b2,b2))

	m1=cross(n1,b2)

	x=dot(n1,n2)
	y=dot(m1,n2)

	dih= -atan2(y,x)*180/pi
	if dih<0: dih+=360
	return dih

def print_ref(name):
	'''Prints reference PDB coordinates for given resname, if present'''
	if name in AAS:
		print ''.join(AAS[name].PDB)
	else: print "Don't have reference for this residue"

def chis(mol,psi=False):
	'''Calculates dihedral angles defining rotamer of PyMolecule object from rotabase'''
	rdat=AAS[mol.name]
	Nrots=len(rdat.rotms)
	quads=[rdat.provide_quad(i) for i in range(Nrots)]
	if psi:
		rseq=SEQ[mol.name]
		psi=['HX2','N','CA','C']
		phi=['HX1','C','CA','N']
		quads=[[rseq.index(x) for x in Y] for Y in [psi,phi]]
	ret= array([dih_calc(mol,qa) for qa in quads])
#	if mol.name=='TYR' and ret[1]>=240.0:
#		ret[1]-=180.0
#		ret[2]=(ret[2]+180.0)%360
	return ret

def chis2code(chis):
	Nmax=3
	#TRZEBA BEDZIE UWZGLEDNIC RANGES! Na razie zakladamy 3
	kat=360/Nmax
	lab=''
	for x in chis:
		lab+='%i'%int(x/kat)
	return lab

def q2e(teta,axis):
	  w=cos(teta/2.0)
	  axis=axis/sqrt(dot(axis,axis))
	  x=sin(teta/2.0)*axis[0]
	  y=sin(teta/2.0)*axis[1]
	  z=sin(teta/2.0)*axis[2]
	  test=x*y+w*z
	  (alfa,beta,gamma)=(0,0,0)
	  if test==0.5:
	    print "OSOBLIWOSC"
	    alfa=2*atan2(x,w)
	    beta=pi/2
	    gamma=0
	  elif test==-0.5:
	    print "OSOBLIWOSC"
	    alfa=-2*atan2(x,w)
	    beta=-pi/2
	    gamma=0
	  else:
	    alfa=atan2(2*x*w+2*y*z,1-2*x*x-2*y*y)
	    beta=-asin(2*x*z-2*w*y)
	    gamma=atan2(2*z*w+2*y*x,1-2*y*y-2*z*z)
	  
	  return [alfa,beta,gamma]

def _make_rotated(mol,i,chi):
	quad=AAS[mol.name].provide_quad(i)
	X=dih_calc(mol,quad)
	axis,to_copy=AAS[mol.name].provide_indices(i)
	tr=mol.coor[axis[1]]
	axis=tr-mol.coor[axis[0]]
	dx=(chi-X)*pi/180
	euler=q2e(dx,axis)
	bak=deepcopy(mol)
	bak.rotate(*euler,P=tr)
	for i in to_copy:
		bak.coor[i]=mol.coor[i]
		bak.CAMM[i]=mol.CAMM[i]
	return bak

def make_rotamer(mol,target_chis):
	bak=deepcopy(mol)
	for i in range(len(target_chis)):
		bak=_make_rotated(bak,i,target_chis[i])
	return bak

def _order_residue(residue,backbone=False,quiet=True):
	rsn=residue.resname
	if backbone:rsn='GLY'
	ret=[]
	for i in SEQ[rsn]:
		if i not in residue:
			if (i=='H') and ('HN' in residue):
				atom=array([ x for x in residue['HN'].get_vector()])
#			elif i[:2]=='HX':continue
			else:
				atom=array([0.0,0.0,0.0])
				if not backbone and not quiet: print 'WARNING: ',i,rsn,' not present in PDB! DO NOT USE corr=True!'
		else:	atom=array([ x for x in residue[i].get_vector()])
		ret.append(atom)
	return ret


	

def _read_camm(residue,base,mode='nearest',expo=2,backbone=False,target_chis=None,target_lab=None): #na razie bez fitowania
	rsn=residue.resname
	if backbone: rsn='GLY'
	if rsn in ['ALA','PRO']: return base[rsn]
	lab,katy=get_rot(residue,backbone)
	if target_lab:lab=target_lab
	if target_chis!=None:katy=target_chis
	ret=base[rsn][lab]
	ret=make_rotamer(ret,katy)
	if mode=='nearest':
		return ret
	elif mode=='interpolate':
		vdict=boxdc(residue)
		sumc=0
		ret.CAMM[:,:]=0.0
		for vert in vdict:
			camm_lab=base[rsn][vdict[vert]]
			nolab=vdict[_nolab(vert)]
			not_camm=base[rsn][nolab]
			not_angles=chis(not_camm)
			da=abs(katy-not_angles)
			da=where(da>180.0,360-da,da)
			P=reduce(lambda x,y:x*y,da)
			camm_lab=make_rotamer(camm_lab,katy)
			sumc+=P
			ret.CAMM+=camm_lab.CAMM*P
		ret.CAMM/=sumc
		return ret
 	elif mode=='interOH':
		if rsn not in ['TYR','SER','THR','CYS']: return ret
 		vdict=boxdc(residue,oh=True)
 		#print vdict
 		#print lab,katy
 		#print '--'
 		sumc=0
 		ret.CAMM[:,:]=0.0
 		for vert in vdict:
 			camm_lab=base[rsn][vdict[vert]]
 			nolab=vdict[_nolab(vert)]
 			not_camm=base[rsn][nolab]
 			not_angles=chis(not_camm)
 			da=abs(katy-not_angles)
 			da=where(da>180.0,360-da,da)
 			P=reduce(lambda x,y:x*y,da)
 			camm_lab=make_rotamer(camm_lab,katy)
 			sumc+=P
 			ret.CAMM+=camm_lab.CAMM*P
 		ret.CAMM/=sumc
 		return ret
	elif mode=='shepard2':
		vdict=boxdc(residue)
		sumc=0
		ret.CAMM[:,:]=0.0
		for vert in vdict:
		   camm_lab=base[rsn][vdict[vert]]
		   angles=chis(camm_lab)
		   da=1-cos((katy-angles)*1.5)
		   if (da==0).all():
				camm_lab=make_rotamer(camm_lab,katy)
				ret.CAMM =camm_lab.CAMM
				sumc=1
				break
		   else:
		   	P=1.0/reduce(lambda x,y:x*y,da)**expo
		   	camm_lab=make_rotamer(camm_lab,katy)
		   	sumc+=P
		   	ret.CAMM+=camm_lab.CAMM*P
		ret.CAMM/=sumc
		return ret
	elif mode=='metric':
		vdict=boxdc(residue)
		sumc=0
		ret.CAMM[:,:]=0.0
		for vert in vdict:
		   camm_lab=base[rsn][vdict[vert]]
		   nolab=vdict[_nolab(vert)]
		   not_camm=base[rsn][nolab]
		   not_angles=chis(not_camm)
		   da=1-cos((katy-not_angles)*1.5)
		   P=reduce(lambda x,y:x*y,da)
		   camm_lab=make_rotamer(camm_lab,katy)
		   sumc+=P
		   ret.CAMM+=camm_lab.CAMM*P
		ret.CAMM/=sumc
		return ret

def read_camm(residue,base,mode='nearest',expo=2.0,corr=False,backbone=False,target_chis=None,target_lab=None,quiet=True):
	'''
Reads CAMM from rotabase basing on given Residue object (Bio.PDB)
residue - Residue object from PDB
base    - dictionary with rotabase
mode    - mode of interpolation
        + nearest - nearest rotamer, with dihedrals adjusted
        + interpolation - linear interpolation between neighbouring rotamers
        + shepard2     - Shepard interpolation
				# expo - exponent used in shepard interpolation
        + interOH  -take the nearest rotamer, interpolate OH group only
corr    - makes a simple correction to final geometry
'''
	rotamer=_read_camm(residue,base,mode,expo,backbone,target_chis,target_lab)
	coordinates=_order_residue(residue,backbone,quiet)
	rsn=residue.resname
	if backbone:rsn='GLY'
	indices=[i for i in range(len(SEQ[rsn])) if SEQ[rsn][i] in ['N','CA','C']]
	ret=fit(coordinates,rotamer,indices)
	if corr:
		for i in range(len(ret.coor)-2):
			if SEQ[rsn][i] in ['O','H']:continue  # leave backbone untouched
			ret.coor[i]=coordinates[i]/0.5291772 # PDB is in Angs
	return ret


def load_camm(residue,target_rsn,target_roti,base,coor_only=False):
	'''
Reads specified rotamer of specified residue into backbone coors of giben Residue object (Bio.PDB)
residue - Residue object from PDB
target_rsn - name of desired residue
target_roti- name of desired rotamer
base    - dictionary with rotabase
coor_only -flag to omit rotation of CAMMs (faster, but good only for geometry)
'''
	rotamer=base[target_rsn][target_roti]
	indices=[i for i in range(len(SEQ[target_rsn])) if SEQ[target_rsn][i] in ['N','CA','C']]
	
	coordinates=[]
	for i in SEQ[target_rsn]:
		if i in ['N','CA','C']:
			coordinates.append([x for x in residue[i].get_vector()])
		else:
			coordinates.append([0.0,0.0,0.0])
	coordinates=array(coordinates)#/0.5291772
	if coor_only:
		ret=fit2(coordinates,rotamer,indices)
	else:
		ret=fit(coordinates,rotamer,indices)
	return ret

def phantom_gly(molcamm,residue,base):
   '''
Generates Gly with dihedrals taken from molcamm object (residue fitted in place).
Used to 'patch' the main chain.
'''
   katy=chis(molcamm,psi=True)
   lab=''
   for a in katy:
      a=int(round(a/60,0))
      if a==6:a=0
      lab+='%i'%a
   gly=read_camm(residue,base,backbone=True,target_lab=lab,target_chis=katy)
   return gly
#======= TEST ==========
if __name__=='__main__':	
	name=argv[1]#'random/THR-0-10.pdb'#argv[1]
	#temporal
	with gzip.open('CAMM/min_bs/rotabase.pkl.gz','rb') as f:base=pic.load(f)
#	for i in ['0','1','2']:
#		with gzip.open('CAMM/THR-%s_camm.pkl.gz'%i,'rb') as f: 
#			cm=pic.load(f)
#			cm.name='THR'
#			base['THR'][i]=cm
	prot=p.get_structure('rpto', name)#'wtf-Y.pdb')
	rsn='TYR'
	aa=Selection.unfold_entities(prot,'R')[0]
	rdat=AAS[rsn]
	with open('wyprost/%s-wyprost2.pdb'%rsn) as f: rdat.PDB_get(f.readlines())
	camm=read_camm(aa,base)
	print chis(camm).round(1)
#	camm_int=read_camm(aa,base,'interpolate')
#	camm_ref=m.PyMultipoles(name[:-4]+'_camm.out','GAMESS')
#	wat=m.PyMultipoles('random/wat-'+name[11:].split('.')[0]+'_camm.out','GAMESS')
#	Eref=m.energy(camm_ref,wat,4)*627.5
#	drop(camm_ref,'WTF-ref.xyz')
#	drop(camm,'WTF-Y-near.xyz')
#	drop(camm_int,'WTF-int.xyz')
#	camm.name='THR'
#	camm_int.name='THR'
#	for x in [camm,camm_int]:
#		X=chis(x)
#		lab=chis2code(X)
#		ene=m.energy(x,wat,4)*627.5
#		print X
#		print lab
#		print ene-Eref
#	Ene_n=m.energy(camm,wat,4)*627.5
#	Ene_i=m.energy(camm_int,wat,4)*627.5
#	camm=make_rotamer(camm,[90.25,101.15])
#	drop(camm,'WTF.xyz')
#	print "%8.4f %8.4f"%(Ene_n-Eref,Ene_i-Eref)

