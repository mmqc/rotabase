

aas_names=['ARG','ASH','ASN','ASP','CYS','GLH','GLN','GLU','HID','HIE','HIP','ILE','LEU','LYS','MET','PHE','SER','THR','TRP','TYR','VAL','GLY']

def load_charges_from_file(filename):
   charges=[]
   with open(filename, 'r') as f:
      for line in f: 
         if not line.startswith('ATOM'): continue
         charges.append(float(line.split()[3]))
   return charges

def set_charges_to_camm_object(camm_object, charges):
   if len(charges)!=len(camm_object.CAMM):
      raise ValueError('Inconsistent number of atoms')
   camm_object.CAMM[:,0]=charges
   camm_object.CAMM[:,1:]=0.0   
