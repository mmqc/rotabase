# 2017.02.03 15:07:50 CET
#Embedded file name: /home/gadget/projects/aabase/topologie.py
from numpy import array, transpose, inner, ones, cross, dot
from math import sqrt, cos, sin, pi, atan2
from copy import deepcopy
from os.path import dirname,realpath

pth=dirname(realpath(__file__)) 

class AACID():
    """ klasa topologii aminowkasu"""

    def dih_calc(self, rotamer):
        qa = rotamer.quad
        b1 = self.coor[qa[1]] - self.coor[qa[0]]
        b2 = self.coor[qa[2]] - self.coor[qa[1]]
        b3 = self.coor[qa[3]] - self.coor[qa[2]]
        n1 = cross(b1, b2)
        n2 = cross(b2, b3)
        n1 = n1 / sqrt(dot(n1, n1))
        n2 = n2 / sqrt(dot(n2, n2))
        b2 = b2 / sqrt(dot(b2, b2))
        m1 = cross(n1, b2)
        x = dot(n1, n2)
        y = dot(m1, n2)
        rotamer.dih = -atan2(y, x) * 180 / pi

    class ROTAMER:
        """Wewnetrzna klasa okreslajaca rotamer"""

        def __init__(self, wiaz, atm, pre = 'HA', nex = 'HB1'):
            self.axis = wiaz
            self.atoms = atm
            self.quad = [pre,
             wiaz[0],
             wiaz[1],
             nex]

    def AddRotamer(self, rotamer):
        self.rotms.append(self.ROTAMER(*rotamer))

    def provide_indices(self, rotid):
        axis = self.rotms[rotid].axis
        rot = self.rotms[rotid].atoms
        axis = [ SEQ[self.name].index(A) for A in axis ]
        rot = [ I for I in range(self.N) if SEQ[self.name][I] not in rot ]
        return (axis, rot)

    def provide_main(self):
		ret=[SEQ[self.name].index(A) for A in ['N','CA','C']]
		return ret
    def provide_quad(self, rotid):
        quad = self.rotms[rotid].quad
        quad = [ SEQ[self.name].index(A) for A in quad ]
        return quad

    def __init__(self, other = [], bond = ['CA', 'CB']):
        self.sdcn = other
        self.rotms = []
        self.main = ['CA',
         'N',
         'H',
         'C',
         'O']
        self.AddRotamer([bond,
         other,
         'N',
         other[3]])
        self.coor = {}
        self.PDB = []
        self.name = ''

    def upgrade(self):
        for i in range(len(self.PDB)):
            at = self.PDB[i][12:16].strip()
            self.PDB[i] = self.PDB[i][:30] + '%8.3f%8.3f%8.3f' % tuple(self.coor[at]) + self.PDB[i][54:]

    def put(self, out):
        self.upgrade()
        for x in self.PDB:
            out.write(x)

    def CAMM_get(self, mol):
        for i in range(len(SEQ[self.name])):
            self.coor[SEQ[self.name][i]] = mol.coor[i] * 0.5291772

        for r in self.rotms:
            self.dih_calc(r)

    def PDB_get(self, dok):
        self.PDB = dok
        self.name = dok[0][17:20]
        for i in self.PDB:
            at = i[12:16].strip()
            self.coor[at] = array([float(i[30:38]), float(i[38:46]), float(i[46:54])])

        self.N = len(self.coor)
        for r in self.rotms:
            self.dih_calc(r)

    def XYZ_get(self, dok):
        self.XYZ = dok[2:]
        for i in self.XYZ:
            if i.strip() != '':
                line = i.split()
                at = line[0]
                self.coor[at] = array([float(line[1]), float(line[2]), float(line[3])])

    def rotate(self, teta, nr):
        """" Usage: self.rotate(kat_w_stopniach, nr_rotameru) """

        def ilo(a, b):
            c = array([0, 0, 0])
            for i in range(3):
                for j in range(3):
                    c[i] = float(c[i] + a[i][j] * b[j])

            return c

        indA = self.rotms[nr].axis[0]
        indB = self.rotms[nr].axis[1]
        wektor = self.coor[indB] - self.coor[indA]
        popr = deepcopy(self.coor[indB])
        w = cos(teta * pi / 360.0)
        wektor = wektor / sqrt(inner(wektor, wektor))
        wektor = wektor * sin(teta * pi / 360.0)
        x, y, z = wektor
        rot = array([[1 - 2 * (y ** 2 + z ** 2), 2 * (x * y - z * w), 2 * (x * z + y * w)], [2 * (x * y + z * w), 1 - 2 * (x ** 2 + z ** 2), 2 * (y * z - x * w)], [2 * (x * z - y * w), 2 * (y * z + x * w), 1 - 2 * (y ** 2 + x ** 2)]])
        for at in self.rotms[nr].atoms:
            self.coor[at] -= self.coor[indA]
            self.coor[at] = inner(rot, self.coor[at])
            self.coor[at] += self.coor[indA]

        self.dih_calc(self.rotms[nr])


def space(mol2, irot = 0, nang = 3, tab = [0]):
    kat = 2 * pi / nang
    for i in range(nang):
        mol = deepcopy(mol2)
        mol.rotate(kat * i, irot)
        if irot < len(tab):
            tab[irot] = i
        else:
            tab.append(i)
        if irot < len(mol.rotms) - 1:
            space(mol, irot + 1, nang, tab)
        else:
            nam = mol.name
            print tab
            for k in range(len(tab)):
                nam += '-%i'

            nam += '.pdb'
            if allow(mol):
                wyn = open(nam % tuple(tab), 'w')
                mol.put(wyn)
                wyn.close()


def allow(mol):
    nazwy = mol.coor.keys()
    permission = True
    for i in range(len(nazwy)):
        at1 = nazwy[i]
        for j in range(i + 1, len(nazwy)):
            at2 = nazwy[j]
            if odl(mol.coor[at1], mol.coor[at2]) < 1.5:
                if (at1, at2) not in mol.bonds and (at2, at1) not in mol.bonds:
                    permission = False

    return permission


TRP_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'CD1',
 'HD1',
 'NE1',
 'HE1',
 'CE2',
 'CZ2',
 'HZ2',
 'CH2',
 'HH2',
 'CZ3',
 'HZ3',
 'CE3',
 'HE3',
 'CD2']
TRP = AACID(other=TRP_TOP)
TRP.AddRotamer([['CB', 'CG'],
 TRP_TOP[3:],
 'CA',
 'CD1'])
SER_TOP = ['CB',
 'HB1',
 'HB2',
 'OG',
 'HG']
SER = AACID(other=SER_TOP)
SER.AddRotamer([['CB', 'OG'],
 SER_TOP[3:],
 'CA',
 'HG'])
CYS_TOP = ['CB',
 'HB1',
 'HB2',
 'SG',
 'HG']
CYS = AACID(other=CYS_TOP)
CYS.AddRotamer([['CB', 'SG'],
 CYS_TOP[3:],
 'CA',
 'HG'])
TYR_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'CD1',
 'HD1',
 'CE1',
 'HE1',
 'CZ',
 'OH',
 'HH',
 'CE2',
 'HE2',
 'CD2',
 'HD2']
TYR = AACID(TYR_TOP)
TYR.AddRotamer([['CB', 'CG'],
 TYR_TOP[3:],
 'CA',
 'CD1'])
TYR.AddRotamer([['CZ', 'OH'],
 TYR_TOP[9:11],
 'CE1',
 'HH'])
PHE_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'CD1',
 'HD1',
 'CE1',
 'HE1',
 'CZ',
 'HZ',
 'CE2',
 'HE2',
 'CD2',
 'HD2']
PHE = AACID(other=PHE_TOP)
PHE.AddRotamer([['CB', 'CG'],
 PHE_TOP[3:],
 'CA',
 'CD1'])
ASP_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'OD1',
 'OD2']
ASP = AACID(other=ASP_TOP)
ASP.AddRotamer([['CB', 'CG'],
 ASP_TOP[3:],
 'CA',
 'OD1'])


ASH_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'OD1',
 'OD2',
 'HD2']
ASH = AACID(other=ASH_TOP)
ASH.AddRotamer([['CB', 'CG'],
 ASH_TOP[3:],
 'CA',
 'OD1'])

ILE_TOP = ['CB',
 'HB',
 'CG2',
 '1HG2',
 '2HG2',
 '3HG2',
 'CG1',
 '1HG1',
 '2HG1',
 'CD',
 'HD1',
 'HD2',
 'HD3']
ILE = AACID(other=ILE_TOP)
ILE.rotms[0].quad[-1] = 'HB'
ILE.AddRotamer([['CB', 'CG1'],
 ILE_TOP[6:],
 'CA',
 'CD'])
HID_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'ND1',
 'HD1',
 'CE1',
 'HE1',
 'NE2',
 'CD2',
 'HD2']
HID = AACID(other=HID_TOP)
HID.AddRotamer([['CB', 'CG'],
 HID_TOP[3:],
 'CA',
 'ND1'])
HIP_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'ND1',
 'HD1',
 'CE1',
 'HE1',
 'NE2',
 'HE2',
 'CD2',
 'HD2']
HIP = AACID(other=HIP_TOP)
HIP.AddRotamer([['CB', 'CG'],
 HIP_TOP[3:],
 'CA',
 'ND1'])
HIE_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'ND1',
 'CE1',
 'HE1',
 'NE2',
 'HE2',
 'CD2',
 'HD2']
HIE = AACID(other=HIE_TOP)
HIE.AddRotamer([['CB', 'CG'],
 HIE_TOP[3:],
 'CA',
 'ND1'])
LEU_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG',
 'CD1',
 '1HD1',
 '2HD1',
 '3HD1',
 'CD2',
 '1HD2',
 '2HD2',
 '3HD2']
LEU = AACID(other=LEU_TOP)
LEU.AddRotamer([['CB', 'CG'],
 LEU_TOP[3:],
 'CA',
 'CD1'])
VAL_TOP = ['CB',
 'HB',
 'CG1',
 '1HG1',
 '2HG1',
 '3HG1',
 'CG2',
 '1HG2',
 '2HG2',
 '3HG2']
VAL = AACID(other=VAL_TOP)
VAL.rotms[0].quad[-1] = 'HB'
GLU_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'OE1',
 'OE2']
GLU = AACID(other=GLU_TOP)
GLU.AddRotamer([['CB', 'CG'],
 GLU_TOP[3:],
 'CA',
 'CD'])
GLU.AddRotamer([['CG', 'CD'],
 GLU_TOP[6:],
 'CB',
 'OE1'])

GLH_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'OE1',
 'OE2',
 'HE2']
GLH = AACID(other=GLH_TOP)
GLH.AddRotamer([['CB', 'CG'],
 GLH_TOP[3:],
 'CA',
 'CD'])
GLH.AddRotamer([['CG', 'CD'],
 GLH_TOP[6:],
 'CB',
 'OE1'])


ASN_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'OD1',
 'ND2',
 '1HD2',
 '2HD2']
ASN = AACID(other=ASN_TOP)
ASN.AddRotamer([['CB', 'CG'],
 ASN_TOP[3:],
 'CA',
 'OD1'])
LYS_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'HD1',
 'HD2',
 'CE',
 'HE1',
 'HE2',
 'NZ',
 'HZ1',
 'HZ2',
 'HZ3']
LYS = AACID(other=LYS_TOP)
LYS.AddRotamer([['CB', 'CG'],
 LYS_TOP[3:],
 'CA',
 'CD'])
LYS.AddRotamer([['CG', 'CD'],
 LYS_TOP[6:],
 'CB',
 'CE'])
LYS.AddRotamer([['CD', 'CE'],
 LYS_TOP[9:],
 'CG',
 'NZ'])
GLN_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'OE1',
 'NE2',
 '1HE2',
 '2HE2']
GLN = AACID(other=GLN_TOP)
GLN.AddRotamer([['CB', 'CG'],
 GLN_TOP[3:],
 'CA',
 'CD'])
GLN.AddRotamer([['CG', 'CD'],
 GLN_TOP[6:],
 'CB',
 'OE1'])
THR_TOP = ['CB',
 'HB',
 'HG1',
 'OG1',
 'CG2',
 '1HG2',
 '2HG2',
 '3HG2']
THR = AACID(other=THR_TOP)
THR.AddRotamer([['CB', 'OG1'],
 ['OG1', 'HG1'],
 'CA',
 'HG1'])
MET_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'SD',
 'CE',
 'HE1',
 'HE2',
 'HE3']
MET = AACID(other=MET_TOP)
MET.AddRotamer([['CB', 'CG'],
 MET_TOP[3:],
 'CA',
 'SD'])
MET.AddRotamer([['CG', 'SD'],
 MET_TOP[6:],
 'CB',
 'CE'])
ARG_TOP = ['CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'HD1',
 'HD2',
 'NE',
 'HE',
 'CZ',
 'NH1',
 'HH11',
 'HH12',
 'NH2',
 'HH21',
 'HH22']
ARG = AACID(other=ARG_TOP)
ARG.AddRotamer([['CB', 'CG'],
 ARG_TOP[3:],
 'CA',
 'CD'])
ARG.AddRotamer([['CG', 'CD'],
 ARG_TOP[6:],
 'CB',
 'NE'])
ARG.AddRotamer([['CD', 'NE'],
 ARG_TOP[9:],
 'CG',
 'CZ'])
ARG.bonds = [('HA', 'CA'),
 ('HE', 'NE'),
 ('HG2', 'HG1'),
 ('HG2', 'CG'),
 ('NE', 'CZ'),
 ('NE', 'CD'),
 ('HG1', 'CG'),
 ('HH22', 'HH21'),
 ('HH22', 'NH2'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('HX1', 'C'),
 ('HH21', 'NH2'),
 ('CB', 'CA'),
 ('CB', 'CG'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('C', 'CA'),
 ('C', 'O'),
 ('H', 'N'),
 ('CA', 'N'),
 ('CG', 'CD'),
 ('CZ', 'NH1'),
 ('CZ', 'NH2'),
 ('NH1', 'HH12'),
 ('NH1', 'HH11'),
 ('CD', 'HD2'),
 ('CD', 'HD1'),
 ('HD2', 'HD1'),
 ('HH12', 'HH11'),
 ('HB1', 'HB2')]
ASN.bonds = [('2HD2', 'ND2'),
 ('2HD2', '1HD2'),
 ('C', 'CA'),
 ('C', 'HX1'),
 ('C', 'O'),
 ('ND2', '1HD2'),
 ('ND2', 'CG'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'OD1'),
 ('CG', 'CB'),
 ('N', 'HX2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
ASP.bonds = [('C', 'CA'),
 ('C', 'HX1'),
 ('C', 'O'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'OD1'),
 ('CG', 'CB'),
 ('CG', 'OD2'),
 ('N', 'HX2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]


ASH.bonds = [('C', 'CA'),
 ('C', 'HX1'),
 ('C', 'O'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'OD1'),
 ('CG', 'CB'),
 ('CG', 'OD2'),
 ('N', 'HX2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2'),
 ('HD2','OD2')]

CYS.bonds = [('C', 'CA'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('N', 'HX2'),
 ('CB', 'SG'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('SG', 'HG'),
 ('HB1', 'HB2')]
GLN.bonds = [('C', 'CA'),
 ('C', 'HX1'),
 ('C', 'O'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('2HE2', '1HE2'),
 ('2HE2', 'NE2'),
 ('CG', 'CD'),
 ('CG', 'HG1'),
 ('CG', 'CB'),
 ('CG', 'HG2'),
 ('N', 'HX2'),
 ('1HE2', 'NE2'),
 ('CD', 'NE2'),
 ('CD', 'OE1'),
 ('HG1', 'HG2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
GLU.bonds = [('C', 'CA'),
 ('C', 'HX1'),
 ('C', 'O'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'CD'),
 ('CG', 'HG1'),
 ('CG', 'CB'),
 ('CG', 'HG2'),
 ('N', 'HX2'),
 ('CD', 'OE2'),
 ('CD', 'OE1'),
 ('HG1', 'HG2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]


GLH.bonds = [('C', 'CA'),
 ('C', 'HX1'),
 ('C', 'O'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'CD'),
 ('CG', 'HG1'),
 ('CG', 'CB'),
 ('CG', 'HG2'),
 ('N', 'HX2'),
 ('CD', 'OE2'),
 ('CD', 'OE1'),
 ('HG1', 'HG2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2'),
 ('HE2','OE2')]

HIP.bonds = [('CD2', 'HD2'),
 ('CD2', 'CG'),
 ('CD2', 'NE2'),
 ('HE2', 'NE2'),
 ('HE1', 'CE1'),
 ('HD1', 'ND1'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'C'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'CB'),
 ('CG', 'ND1'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('N', 'HX2'),
 ('NE2', 'CE1'),
 ('CE1', 'ND1'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
HIE.bonds = [('CD2', 'HD2'),
 ('CD2', 'CG'),
 ('CD2', 'NE2'),
 ('HE1', 'CE1'),
 ('HE2', 'NE2'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'C'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'CB'),
 ('CG', 'ND1'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('N', 'HX2'),
 ('NE2', 'CE1'),
 ('CE1', 'ND1'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
HID.bonds = [('CD2', 'HD2'),
 ('CD2', 'CG'),
 ('CD2', 'NE2'),
 ('HE1', 'CE1'),
 ('HD1', 'ND1'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'C'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'CB'),
 ('CG', 'ND1'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('N', 'HX2'),
 ('NE2', 'CE1'),
 ('CE1', 'ND1'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
ILE.bonds = [('2HG2', 'CG2'),
 ('2HG2', 'HD1'),
 ('2HG2', '1HG2'),
 ('2HG2', '3HG2'),
 ('2HG1', '1HG1'),
 ('2HG1', 'CG1'),
 ('CG2', '1HG2'),
 ('CG2', 'CB'),
 ('CG2', '3HG2'),
 ('HD3', 'HD2'),
 ('HD3', 'HD1'),
 ('HD3', 'CD'),
 ('HD2', 'HD1'),
 ('HD2', 'CD'),
 ('HD1', 'CD'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('H', 'N'),
 ('CA', 'C'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('1HG1', 'CG1'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('CG1', 'CD'),
 ('CG1', 'CB'),
 ('1HG2', '3HG2'),
 ('CB', 'HB')]
LEU.bonds = [('2HD2', '1HD2'),
 ('2HD2', '3HD2'),
 ('2HD2', 'CD2'),
 ('1HD1', '2HD1'),
 ('1HD1', '3HD1'),
 ('1HD1', 'CD1'),
 ('2HD1', '3HD1'),
 ('2HD1', 'CD1'),
 ('1HD2', '3HD2'),
 ('1HD2', 'CD2'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'C'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'CD1'),
 ('CG', 'CD2'),
 ('CG', 'CB'),
 ('CG', 'HG'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('N', 'HX2'),
 ('3HD1', 'CD1'),
 ('3HD2', 'CD2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
LYS.bonds = [('HA', 'CA'),
 ('HE1', 'HE2'),
 ('HE1', 'NZ'),
 ('HE1', 'HZ1'),
 ('HE1', 'CE'),
 ('HE2', 'NZ'),
 ('HE2', 'HZ2'),
 ('HE2', 'CE'),
 ('HG2', 'HG1'),
 ('HG2', 'CG'),
 ('HG2', 'CD'),
 ('HX1', 'C'),
 ('HG1', 'CG'),
 ('HG1', 'CD'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('NZ', 'HZ1'),
 ('NZ', 'HZ3'),
 ('NZ', 'HZ2'),
 ('NZ', 'CE'),
 ('HZ1', 'HZ3'),
 ('HZ1', 'HZ2'),
 ('HZ1', 'CE'),
 ('HZ3', 'HZ2'),
 ('HZ3', 'CE'),
 ('HZ2', 'CE'),
 ('CB', 'CA'),
 ('CB', 'CG'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('C', 'CA'),
 ('C', 'O'),
 ('H', 'N'),
 ('CA', 'N'),
 ('CG', 'CD'),
 ('CG', 'HD2'),
 ('CG', 'HD1'),
 ('CE', 'CD'),
 ('CD', 'HD2'),
 ('CD', 'HD1'),
 ('HD2', 'HD1'),
 ('HB1', 'HB2')]
MET.bonds = [('O', 'C'),
 ('C', 'CA'),
 ('C', 'HX1'),
 ('HE1', 'HE2'),
 ('HE1', 'HE3'),
 ('HE1', 'CE'),
 ('HE2', 'HE3'),
 ('HE2', 'CE'),
 ('HE3', 'CE'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('CG', 'SD'),
 ('CG', 'HG1'),
 ('CG', 'CB'),
 ('CG', 'HG2'),
 ('CE', 'SD'),
 ('N', 'HX2'),
 ('HG1', 'HG2'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
PHE.bonds = [('HZ', 'CZ'),
 ('CD1', 'CG'),
 ('CD1', 'CE1'),
 ('CD1', 'HD1'),
 ('CD2', 'CG'),
 ('CD2', 'CE2'),
 ('CD2', 'HD2'),
 ('HA', 'CA'),
 ('HE1', 'CE1'),
 ('HE2', 'CE2'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('HX1', 'C'),
 ('CB', 'CA'),
 ('CB', 'CG'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('C', 'CA'),
 ('C', 'O'),
 ('H', 'N'),
 ('CA', 'N'),
 ('CZ', 'CE2'),
 ('CZ', 'CE1'),
 ('HB1', 'HB2')]
SER.bonds = [('C', 'CA'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('OG', 'CB'),
 ('OG', 'HG'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('N', 'HX2'),
 ('CB', 'HB1'),
 ('CB', 'HG'),
 ('CB', 'HB2'),
 ('HB1', 'HB2')]
THR.bonds = [('2HG2', 'CG2'),
 ('2HG2', '1HG2'),
 ('2HG2', '3HG2'),
 ('C', 'CA'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('CG2', '1HG2'),
 ('CG2', 'CB'),
 ('CG2', '3HG2'),
 ('H', 'N'),
 ('H', 'HX2'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('OG1', 'HG1'),
 ('OG1', 'CB'),
 ('N', 'HX2'),
 ('1HG2', '3HG2'),
 ('HG1', 'CB'),
 ('CB', 'HB')]
TRP.bonds = [('HH2', 'CH2'),
 ('CZ2', 'CH2'),
 ('CZ2', 'HZ2'),
 ('CZ2', 'CE2'),
 ('CZ3', 'CH2'),
 ('CZ3', 'HZ3'),
 ('CZ3', 'CE3'),
 ('CD1', 'CG'),
 ('CD1', 'HD1'),
 ('CD1', 'NE1'),
 ('CD2', 'CG'),
 ('CD2', 'CE3'),
 ('CD2', 'CE2'),
 ('HA', 'CA'),
 ('HE1', 'NE1'),
 ('HE3', 'CE3'),
 ('HX1', 'C'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('CB', 'CA'),
 ('CB', 'CG'),
 ('CB', 'HB2'),
 ('CB', 'HB1'),
 ('C', 'CA'),
 ('C', 'O'),
 ('H', 'N'),
 ('CA', 'N'),
 ('CE2', 'NE1'),
 ('HB2', 'HB1')]
TYR.bonds = [('HH', 'CZ'),
 ('HH', 'OH'),
 ('CD1', 'CG'),
 ('CD1', 'CE1'),
 ('CD1', 'HD1'),
 ('CD2', 'CG'),
 ('CD2', 'CE2'),
 ('CD2', 'HD2'),
 ('HA', 'CA'),
 ('HE1', 'CE1'),
 ('HE2', 'CE2'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('HX1', 'C'),
 ('CB', 'CA'),
 ('CB', 'CG'),
 ('CB', 'HB1'),
 ('CB', 'HB2'),
 ('C', 'CA'),
 ('C', 'O'),
 ('H', 'N'),
 ('CA', 'N'),
 ('CZ', 'CE2'),
 ('CZ', 'CE1'),
 ('CZ', 'OH'),
 ('HB1', 'HB2')]
VAL.bonds = [('2HG2', 'CG2'),
 ('2HG2', '1HG2'),
 ('2HG2', '3HG2'),
 ('2HG1', '1HG1'),
 ('2HG1', 'CG1'),
 ('2HG1', '3HG1'),
 ('CG2', '1HG2'),
 ('CG2', 'CB'),
 ('CG2', '3HG2'),
 ('HX2', 'H'),
 ('HX2', 'N'),
 ('H', 'N'),
 ('H', '1HG2'),
 ('CA', 'C'),
 ('CA', 'N'),
 ('CA', 'CB'),
 ('CA', 'HA'),
 ('1HG1', 'CG1'),
 ('1HG1', '3HG1'),
 ('C', 'O'),
 ('C', 'HX1'),
 ('CG1', '3HG1'),
 ('CG1', 'CB'),
 ('1HG2', '3HG2'),
 ('CB', 'HB')]
AS = [ARG,
 ASN,
 ASH,
 ASP,
 CYS,
 GLN,
 GLU,
 GLH,
 HID,
 HIE,
 HIP,
 ILE,
 LEU,
 LYS,
 MET,
 PHE,
 SER,
 THR,
 TRP,
 TYR,
 VAL]
AS_N = ['arg',
 'asn',
 'asp',
 'ash',
 'cys',
 'gln',
 'glu',
 'glh',
 'hid',
 'hie',
 'hip',
 'ile',
 'leu',
 'lys',
 'met',
 'phe',
 'ser',
 'thr',
 'trp',
 'tyr',
 'val']
AS_N=[x.upper() for x in AS_N]
AAS = dict(zip(AS_N, AS))
for i in AAS: 
	AAS[i].PDB_get(open(pth+'/wyprost/%s-wyprost2.pdb'%i,'r').readlines())

def odl(a, b):
    w = a - b
    return sqrt(dot(w, w))


ARG_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'HD1',
 'HD2',
 'NE',
 'HE',
 'CZ',
 'NH1',
 'HH11',
 'HH12',
 'NH2',
 'HH21',
 'HH22',
 'C',
 'O',
 'HX1',
 'HX2']
ASN_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'OD1',
 'ND2',
 '1HD2',
 '2HD2',
 'C',
 'O',
 'HX1',
 'HX2']
ASP_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'OD1',
 'OD2',
 'C',
 'O',
 'HX1',
 'HX2']


ASH_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'OD1',
 'OD2',
 'HD2',
 'C',
 'O',
 'HX1',
 'HX2']

CYS_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'SG',
 'HG',
 'C',
 'O',
 'HX1',
 'HX2']
GLN_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'OE1',
 'NE2',
 '1HE2',
 '2HE2',
 'C',
 'O',
 'HX1',
 'HX2']
GLU_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'OE1',
 'OE2',
 'C',
 'O',
 'HX1',
 'HX2']

GLH_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'OE1',
 'OE2',
 'HE2',
 'C',
 'O',
 'HX1',
 'HX2']


HID_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'ND1',
 'HD1',
 'CG',
 'CE1',
 'HE1',
 'NE2',
 'CD2',
 'HD2',
 'C',
 'O',
 'HX1',
 'HX2']
HIE_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'ND1',
 'CE1',
 'HE1',
 'NE2',
 'HE2',
 'CD2',
 'HD2',
 'C',
 'O',
 'HX1',
 'HX2']
ILE_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB',
 'CG2',
 '1HG2',
 '2HG2',
 '3HG2',
 'CG1',
 '1HG1',
 '2HG1',
 'CD',
 'HD1',
 'HD2',
 'HD3',
 'C',
 'O',
 'HX1',
 'HX2']
LEU_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG',
 'CD1',
 '1HD1',
 '2HD1',
 '3HD1',
 'CD2',
 '1HD2',
 '2HD2',
 '3HD2',
 'C',
 'O',
 'HX1',
 'HX2']
LYS_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'CD',
 'HD1',
 'HD2',
 'CE',
 'HE1',
 'HE2',
 'NZ',
 'HZ1',
 'HZ2',
 'HZ3',
 'C',
 'O',
 'HX1',
 'HX2']
MET_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'HG1',
 'HG2',
 'SD',
 'CE',
 'HE1',
 'HE2',
 'HE3',
 'C',
 'O',
 'HX1',
 'HX2']
PHE_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'CD1',
 'HD1',
 'CE1',
 'HE1',
 'CZ',
 'HZ',
 'CE2',
 'HE2',
 'CD2',
 'HD2',
 'C',
 'O',
 'HX1',
 'HX2']
SER_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'OG',
 'HG',
 'C',
 'O',
 'HX1',
 'HX2']
THR_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB',
 'CG2',
 '1HG2',
 '2HG2',
 '3HG2',
 'OG1',
 'HG1',
 'C',
 'O',
 'HX1',
 'HX2']
TRP_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'CD1',
 'HD1',
 'NE1',
 'HE1',
 'CE2',
 'CZ2',
 'HZ2',
 'CH2',
 'HH2',
 'CZ3',
 'HZ3',
 'CE3',
 'HE3',
 'CD2',
 'C',
 'O',
 'HX1',
 'HX2']
TYR_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB1',
 'HB2',
 'CG',
 'CD1',
 'HD1',
 'CE1',
 'HE1',
 'CZ',
 'OH',
 'HH',
 'CE2',
 'HE2',
 'CD2',
 'HD2',
 'C',
 'O',
 'HX1',
 'HX2']
HIP_seq=[\
'N',  
'H',  
'CA', 
'HA', 
'CB', 
'HB1',
'HB2',
'CG', 
'ND1',
'HD1',
'CE1',
'HE1',
'NE2',
'HE2',
'CD2',
'HD2',
'C',  
'O',  
'HX1',
'HX2']
VAL_seq = ['N',
 'H',
 'CA',
 'HA',
 'CB',
 'HB',
 'CG1',
 '1HG1',
 '2HG1',
 '3HG1',
 'CG2',
 '1HG2',
 '2HG2',
 '3HG2',
 'C',
 'O',
 'HX1',
 'HX2']
ALA_seq=[ \
'N',
'H',
'CA',
'HA',
'CB',
'HB1',
'HB2',
'HB3',
'C',
'O',
'HX1',
'HX2']
GLY_seq=[ \
'N',
'H',
'CA',
'HA1',
'HA2',
'C',
'O',
'HX1',
'HX2']
PRO_seq=[ \
'N',
'CD',
'HD1',
'HD2',
'CA',
'HA',
'CB',
'HB1',
'HB2',
'CG',
'HG1',
'HG2',
'C',
'O',
'HX1',
'HX2']

SEQ = {'ALA':ALA_seq,
 'GLY':GLY_seq,
 'PRO':PRO_seq,
 'ARG': ARG_seq,
 'ASN': ASN_seq,
 'ASP': ASP_seq,
 'ASH': ASH_seq,
 'CYS': CYS_seq,
 'GLN': GLN_seq,
 'GLU': GLU_seq,
 'GLH': GLH_seq,
 'HID': HID_seq,
 'HIE': HIE_seq,
 'ILE': ILE_seq,
 'LEU': LEU_seq,
 'LYS': LYS_seq,
 'MET': MET_seq,
 'PHE': PHE_seq,
 'SER': SER_seq,
 'THR': THR_seq,
 'TRP': TRP_seq,
 'TYR': TYR_seq,
 'VAL': VAL_seq,
 'HIP': HIP_seq}
#+++ okay decompyling ../topologie.pyc 
# decompiled 1 files: 1 okay, 0 failed, 0 verify failed
# 2017.02.03 15:07:51 CET

#GLY
GLY_seq=[ 'HX1',  
 'N',   
 'CA',  
 'C',   
 'HX2',  
 'O',   
 'HN',  
 'HA1', 
 'HA2',] 

class gly():
	def __init__(self):
		self.rotms=[3,2,1,0],[1,2,3,4]
		self.copy=[3,4,5,7,8],[0,1,6,7,8]
	def provide_quad(self,i):return self.rotms[i]
	def provide_indices(self,i):return self.rotms[i][1:3],self.copy[i]

SEQ['GLY']=GLY_seq
AAS['GLY']=gly()
AAS['G']=AAS['GLY']
