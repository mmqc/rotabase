import numpy as np
from rotabase import *
from time import time

mtp=multipoles

def make_combination_list(list_of_sets):
   result=[]
   for current_set in list_of_sets:
      if result==[]:
         result=[[element] for element in current_set]
      else:
         new_result=[]
         for combination in result:
            new_result+=[combination + [element] for element in current_set] 
         result=new_result
   return result


def _mindist(X,Y): # X,Y- arrays of shape (NX,3) and (NY,3)
    '''finds the shortest distance between sets of coordinates'''
    Dmin=-1
    for x in X:
        for y in Y:
            d=x-y
            d=np.sqrt(d.dot(d))
            if d<Dmin or Dmin==-1: 
               Dmin=d
    return Dmin


def preprare_backbone_xyz(protein, ligands_to_include=[], return_resid_dict=True, model=0):
    atoms = Selection.unfold_entities(protein[model],'A')
    backbone=[]
    idx=0
    resid_dict={}
    for A in atoms:
        a_name = A.name
        if a_name not in ['N','CA','C','O']:
            continue
        a_resid = A.get_parent().id[1]  # get the resid
        a_chain = A.get_parent().get_parent().id
        identifier = '%s_%i'%(a_chain,a_resid)
        if identifier not in resid_dict:
            resid_dict[identifier]=[]
        xyz = [x/0.5291772 for x in A.get_vector()]  # get coordinates, change units from A to au
        backbone.append(xyz)
        resid_dict[identifier].append(idx)
        idx+=1

    for ligand in ligands_to_include:
      for x in ligand.coor: 
           backbone.append(x)
    backbone=np.array(backbone)
    if return_resid_dict:
      return backbone, resid_dict
    else:
      return backbone


def load_all_rotamers(residue, target_rsn, base, coor_only=False, backbone=None, assign_resp=False, clash_th_au=4):
   '''    
    Fits all rotamers of residue 'target_rsn' into position given by residue (ResidueObject).
    Excludes those that have a clash with the protein backbone.
    
    Parameters
    ----------
    residue: BioPython ResidueObject
        needed to specify 3D position and parents
    target_rsn: string
        name of amino acid to be loaded
    base: dictionary
        the Rotabase
    coor_only: bool, optional
        Toggles transformation of CAMM arrays when fitting the position of rotamer.
        When False, the objects should not be used for any calculation from multipole expansion.
        Keep in mind that the transformation of CAMMs may take substancial time.
    backbone: tuple (numpy_array, dictionary), optional
        Tuple of backbone coordinates and the dictionary mapping residues to backbone indices
        The same as the default resut of prepare_backbone_xyz.
        If not provided, it will be generated based on residue's parents.
    assign_resp: bool, optional
        If True, resulting CAMM objects will be assign with Charmm27 RESP charges.
        Other multipole moments cleared.
    clash_th_au: float, optional
        Treshold for short-contact detection between rotamer and the backbone.
    
    Returns
    -------
    out : dictionary of CAMM objects of fitted rotamers, indexed by rotamer_IDs as in rotabase
    '''
   
   if backbone is None:
      model = residue.get_parent().get_parent()
      structure = model.get_parent()
      backbone_coors, indices_dict = preprare_backbone_xyz(structure, model=model.id)
   else:
      backbone_coors, indices_dict = backbone

   residue_id = '%s_%i'%(residue.get_parent().id, residue.id[1])
   result = {}
   idx_to_include = [x for x in range(backbone_coors.shape[0]) if x not in indices_dict[residue_id]]
   backbone_included = backbone_coors[idx_to_include,:]
   
   for rotamer_id in base[target_rsn]:
      aa_camm = load_camm(residue, target_rsn, rotamer_id, base, coor_only=coor_only)
      dmin=_mindist(aa_camm.coor[7:-4],backbone_included) 
      if dmin<=clash_th_au:
         continue
      if assign_resp:
        set_charges_to_camm_object(aa_camm, aas_charmm27_crg[target_rsn])
      result[rotamer_id]=aa_camm

   return result


def prepare_structure_for_scan(path_to_pdb, residue_list, ligand_paths={}, model=0, coor_only=True, clash_th_au=4, assign_resp=False): 
   '''
   Function intended to prepare structures for interaction energy analysis, common for all 'scans'.
     
   =========
   Input:
      path_to_pdb: str, required
         Path to considered PDB file
      residue_list: list of strings, required
         List of residues, for wich CAMMs are to be loaded. Each element must follow the pattern: resname_chain_resid
         If a given residue is native to the structure (mutation is not considered), the resname may be ommited.
      ligands: dictionary, optional
         A dict with keys being the identifiers to be used, and corresponding elements denoting path of apropriate file.
         Works with both output files from GAMESS and pickled PyMultipoles objects.
      model: int, default=0
         Index of model to be used. If there's only one model (snapshot) in your PDB, it does not matter.
      coor_only: bool, optional
         Toggles mode in which CAMM arrays are not transformed. Saves a lot of time, but multipole moments are useless.
         You should not change this flag unless you are sure to load all rotamers' multipoles.
      assign_resp: bool, optional
         Whether or not assign resp charges to aminoacids.
   
   =========
   Output:
      aminoacids: dict of dicts
         Two-level dictionary containing PyMolecule objects. 
         Firts level keys are the residue identifiers, while the second level keys are rotamer identifiers.
      resdue_obcets: dict
         dictionary providing original residue objects for further evaluation
      ligands: dict
         Dictionary containing PyMultipoles objects of ligands.
   '''
   parser = PDBParser(QUIET=True)
   t=time()
   prot = parser.get_structure('prot', path_to_pdb)
   load_pdb_time = time()-t
   ligands={}
   
   t=time()
   #prepare_ligands, if provided
   for name, path in ligand_paths.items():
      if path[-3:]=='out':
         ligand_camm = mtp.PyMultipoles(path, 'GAMESS')
      else:
         ligand_camm = load(path)
      ligands[name]=ligand_camm
   
   load_ligands_time = time()-t
   #prepare backbone
   t=time()
   backbone_coors, indices_dict = preprare_backbone_xyz(prot, model=model, ligands_to_include=ligands.values())
   prepare_backbone_time = time()-t   

   #load stuff
   aminoacids = {}
   residue_objects = {}
   t=time()
   for residue_identifier in residue_list:
      residue_identifier_parts = residue_identifier.upper().split('_')
      Nparts = len(residue_identifier_parts)
      resid = int(residue_identifier_parts[-1])
      if Nparts==3:
         resname, chain = residue_identifier_parts[:2]
      elif Nparts==2:
         chain = residue_identifier_parts[0]
         resname = None
      else:
         resname = None
         chain = prot[model].child_dict.keys()[0]
      residue = prot[model][chain][resid]
      if resname is None:
         resname = residue.resname
      rotamers = load_all_rotamers(residue, resname, base, coor_only=coor_only, 
                                   backbone=(backbone_coors, indices_dict), 
                                   assign_resp=assign_resp, clash_th_au=clash_th_au)
      aminoacids[residue_identifier.upper()] = rotamers
      residue_objects[residue_identifier.upper()] = residue

   prepare_residues_time = time()-t
   
   print('load_pdb', load_pdb_time)
   print('load_ligands', load_ligands_time)
   print('prepare_backbone', prepare_backbone_time)
   print('prepare_residues', prepare_residues_time)

   return aminoacids, residue_objects, ligands
      
      
class EnergyProvider():
   '''
      Class intended for dynamic storage of interaction energies between rotamers.
      Energies are calculated only when needed and the result is stored inside given object.
   '''
   def __init__(self, aminoacid_dict={}, other_ligands={}, energy_function=mtp.energy, energy_function_kwargs={}, clash_th_au=4):
      '''
       Input parameters:
         aminoacid_dict: dictionary of dictionaries:
            First level keys:  strings RSN_C_RID
            Second level keys: strings denoting rotamer ID
            values:            loaded  PyMolecule objects
         other_ligands: dictionary with additional PyMolecule objects (keyed with str names)
         L: multipole expansion level
         clash_th_au: cotuff for interresidue distance in atomic units
      '''
      self.aminoacids = aminoacid_dict
      self.other_ligands = other_ligands
      self.clash = clash_th_au
      self.energy_function = energy_function
      self.energy_function_kwargs = energy_function_kwargs 
      self.energies = {}


   def _get_object(self, key):
      if key in self.other_ligands:
         rotamer = self.other_ligands[key]
         xyz = rotamer.coor
      elif ':' in key:
         resid_key, rotid = key.split(':')
         rotamers = self.aminoacids.get(resid_key, 'NONE')
         if rotamers=='NONE':
            raise KeyError('Unknown residue name %s'%resid_key)
         rotamer = rotamers.get(rotid, 'NONE')
         if rotamer=='NONE':
            raise KeyError('Unknown rotamer key for residue %s'%resid_key)
         xyz = rotamer.coor[7:-4]
         rotamer
      else:
         raise KeyError('Unknown molecule: %s'%key)
      return rotamer, xyz


   def get_pair_energy(self, keyA, keyB):
      '''
         Get interaction energy between entities denoted by keywords keyA and keyB.
         In the case of aminoacid residues, key is defined as a string: RSN_A_RID:ROTID.
      '''
      keys = [keyA, keyB]
      keys.sort()
      key='-'.join(keys)
      
      if key in self.energies:
         energy = self.energies[key]
      else:
         molA, xyzA = self._get_object(keyA)
         molB, xyzB = self._get_object(keyB)
         contact = _mindist(xyzA, xyzB)
         if contact <= self.clash:
            energy = 999
         else: 
            energy = self.energy_function(molA, molB, **self.energy_function_kwargs)
            self.energies[key]=energy
      return energy


   def get_configuration_energy(self, keys_list):
      E=0
      N=len(keys_list)
      for i in range(N):
         for j in range(i+1,N):
            E+=self.get_pair_energy(keys_list[i], keys_list[j])
      return E
   
   
def find_extreme_rotamers(provider, aminoacid_key, other_key, exclude_clash=True):
    Emin =  9999
    Emax = -9999
    max_rotid=''
    min_rotid=''
    for rotamer_id in provider.aminoacids[aminoacid_key].keys():
       key=aminoacid_key+':'+rotamer_id
       E = provider.get_pair_energy(key, other_key)
       if E==999:continue
       if E<Emin:
          Emin=E
          min_rotid = key
       if E>Emax:
          Emax=E
          max_rotid = key
      
    return {'min':(Emin, min_rotid), 'max':(Emax, max_rotid)}


def find_extreme_rotamer_sets(provider, ligand_key):
   '''Also known as 'single scan' '''
   Emin, Emax = 0, 0
   min_set, max_set = [], []
   for aminoacid_key in provider.aminoacids:
      min_max = find_extreme_rotamers(provider, aminoacid_key, ligand_key)
      a_min_E, a_min_id = min_max['min']
      a_max_E, a_max_id = min_max['max']
      if a_min_E<9999:
         min_set.append(a_min_id)
         Emin+=a_min_E
      if a_max_E>-9999:
         max_set.append(a_max_id)
         Emax+=a_max_E
   
   return {'min':(Emin,min_set), 'max':(Emax,max_set)}
   
   
def multiskan(provider, residues_key_list, return_all=False):
   identifiers = []
   for key in residues_key_list:
      variants_in_position = []
      if key in provider.aminoacids:
         for rotamer_id in provider.aminoacids[key]:
            variants_in_position.append(key+':'+rotamer_id)
      else:
         variants_in_position.append(key)
      identifiers.append(variants_in_position)
   
   configurations = make_combination_list(identifiers)
   Emin = 9999
   min_configuration = []
   all_data=[]
   for configuration in configurations:
      E = provider.get_configuration_energy(configuration)
      if E<Emin:
         Emin=E
         min_configuration = configuration
      if return_all:
         all_data.append(E)
   if return_all:
      return all_data, configurations
   else:
      return Emin, min_configuration


def check_goldstein_single(provider, residue_key, rotamer_id, th=0, aminoacid_key_list=None):
   '''
   Checks whether a rotmaer is ok according to goldstein cryterion.
   Self-energy neglected.'''
   
   if residue_key not in provider.aminoacids:
      return True

   if aminoacid_key_list is None:
      aminoacid_key_list = list(provider.aminoacids.keys())
   
   query_key = '%s:%s'%(residue_key, rotamer_id)
   for other_rotamer in provider.aminoacids[residue_key]:
      if other_rotamer==rotamer_id:
         continue 
      alternative_key = '%s:%s'%(residue_key, other_rotamer)
      energy_total = 0
      for key in aminoacid_key_list:
         if key==residue_key:
            continue
         residue_difference=99999
         for rotid in provider.aminoacids[key]:
            other_aa_key = '%s:%s'%(key, rotid)
            query_interaction = provider.get_pair_energy(query_key, other_aa_key)
            alternative_interaction = provider.get_pair_energy(alternative_key, other_aa_key)
            diff = query_interaction - alternative_interaction
            if diff<residue_difference:
               residue_difference=diff
         energy_total+=residue_difference
      for key in provider.other_ligands:
         query_interaction = provider.get_pair_energy(query_key, key)
         alternative_interaction = provider.get_pair_energy(alternative_key, key)
         energy_total+=(query_interaction-alternative_interaction)
      if energy_total > th:
         return False
   
   return True


def _check_identifier_present(provider, identifier):
   if identifier in provider.other_ligands:
      return True
   resname, rotid = identifier.split(':')
   if resname in provider.aminoacids:
      return rotid in provider.aminoacids[resname]
   return False


def check_goldstein_pair(provider, identifier1, identifier2, th=0):
   '''
   Checks whether a pair of rotmaers is ok according to goldstein cryterion.
   Self-energy neglected.'''
   
   assert _check_identifier_present(provider, identifier1)
   assert _check_identifier_present(provider, identifier2)
   assert (':' in identifier1) and (':' in identifier2)
   
   U12 = provider.get_pair_energy(identifier1, identifier2)
   rsn1, rotid1 = identifier1.split(':')
   rsn2, rotid2 = identifier2.split(':')
   
   for alter_rotid1 in provider.aminoacids[rsn1]:
      alter_key1 = '%s:%s'%(rsn1, alter_rotid1)
      for alter_rotid2 in provider.aminoacids[rsn2]:
         if (alter_rotid2==rotid2) and (alter_rotid1==rotid1):
            continue
    
         alter_key2 = '%s:%s'%(rsn2, alter_rotid2)
         Ua1a2 = provider.get_pair_energy(alter_key1, alter_key2)
         total_energy = U12-Ua1ua2
         for key in provider.aminoacids:
            if key in [rsn1, rsn2]:
               continue
            pair_diff=99999
            for rotid in provider.aminoacids[key]:
               new_key = '%s:%s'%(key, rotid)
               u1x = provider.get_pair_energy(identifier1, new_key)
               u2x = provider.get_pair_energy(identifier2, new_key)
               ua1x = provider.get_pair_energy(alter_key1, new_key)
               ua2x = provider.get_pair_energy(alter_key2, new_key)
               diff = u1x+u2x - (ua1x+ua2x)
               if pair_diff>diff:
                  pair_diff=diff
            total_energy+= pair_diff
         for key in provider.other_ligands:
            u1x = provider.get_pair_energy(identifier1, key)
            u2x = provider.get_pair_energy(identifier2, key)
            ua1x = provider.get_pair_energy(alter_key1, key)
            ua2x = provider.get_pair_energy(alter_key2, key)
            diff = u1x+u2x - (ua1x+ua2x)
            total_energy+= diff
            
         if total_energy > th:
            return False
   return True
                  
      
#inh_charges = load_charges_from_file('inhE7W.str')
#set_charges_to_camm_object(inh_resp, inh_charges)
