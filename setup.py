#!/usr/bin/python

from distutils.core import setup

base_files=['bbindep2015.txt','rotamery-def','data/rotabase.pkl.gz','data/rotabase-mp2.pkl.gz','data/glys.pkl.gz','data/glys-mp2.pkl.gz']
module_files=['_fit_lib.py','_rotacamm.py','_rotanal.py','_topologie.py']
pdb_dir='wyprost'
pdb_files=['ARG-wyprost2.pdb','ASH-wyprost2.pdb','ASN-wyprost2.pdb', \
           'ASP-wyprost2.pdb','CYS-wyprost2.pdb','GLH-wyprost2.pdb', \
           'GLN-wyprost2.pdb','GLU-wyprost2.pdb','HID-wyprost2.pdb', \
           'HIE-wyprost2.pdb','HIP-wyprost2.pdb','ILE-wyprost2.pdb', \
           'LEU-wyprost2.pdb','LYS-wyprost2.pdb','MET-wyprost2.pdb', \
           'PHE-wyprost2.pdb','SER-wyprost2.pdb','THR-wyprost2.pdb', \
           'TRP-wyprost2.pdb','TYR-wyprost2.pdb','VAL-wyprost2.pdb']
setup(
  name='rotabase',
  version='1.3',
  author = "Wiktor 'Nanotekton' Beker",
  author_email = "wiktor.beker@pwr.edu.pl",
  url = "http://156.17.246.1/CAMM",
  description = "Package for working CAMM rotamer database",
  packages = ['rotabase'],
  package_dir = {'rotabase':''},
  package_data = {'rotabase':[pdb_dir + '/' +x for x in pdb_files] + module_files + base_files}
)
